DB_URL = 'mysql+pymysql://flap:flapmyport@localhost/traps'
MAIL_FROM = 'trap_harvester@yourdomain.com'
MAIL_TO = ['user_first@yourdomain.com','user_second@yourdomain.com']
SNMP_COMMUNITY = 'your_SNMP_community'
FLAP_THR_MINUTES = 15
FLAP_THR_COUNT = 20
